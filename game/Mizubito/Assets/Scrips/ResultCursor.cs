﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultCursor : MonoBehaviour
{
    //状態
    enum POSITION
    {
        RIGHT,  //右
        LEFT    //左
    }

    //カーソルがいる位置を状態として持つ
    private POSITION currentPos_ = POSITION.LEFT;

    //カーソルの移動量
    private const float CURSOR_MOVE_X = 1200.0f;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D)) && currentPos_ == POSITION.LEFT)
        {
            //右へ移動
            transform.Translate(CURSOR_MOVE_X, 0, 0);

            currentPos_ = POSITION.RIGHT;
        }
        else if ((Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A)) && currentPos_ == POSITION.RIGHT)
        {
            //左へ移動
            transform.Translate(-CURSOR_MOVE_X, 0, 0);

            currentPos_ = POSITION.LEFT;
        }

    }

    //現在の位置を渡す
    public int GetPosition()
    {
        return (int)currentPos_;
    }
}
