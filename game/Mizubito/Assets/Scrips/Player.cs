﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //移動スピード
    private float MOVE_SPEED = 6.0f;

    //ダッシュ力
    private const float DASH_POWER = 2.0f;

    //ジャンプ力
    private const float JUMP_POWER = 6f;

    //プレイヤー初期の向きを修正用
    private const int ROTATE_FRONT = 90;

    //プレイヤーが死ぬ高さ
    private const float DEAD_LINE = -600;

    //プレイヤー回転用
    private const int TURN = 180;

    //FPS値
    private const int FLAME_MAX = 60;

    //重力
    private const float GRAVITY = -10.0f;

    //体力の下限
    private const int DEAD_HP = 0;

    //初期位置
    private const float SPAWN_POS = 420.0f;

    //自分の半分の大きさ
    private const float RADIUS = 60.0f;

    //ステージの左端
    private const float STAGE_L_EDGE = RADIUS;

    //ステージの右端
    private const float STAGE_R_EDGE = 10000;

    //スキルカーソルオブジェクト
    private GameObject skillCursor_;
    
    //private Ray rayR_;                  // 飛ばすレイ右端
    //private Ray rayL_;                  // 飛ばすレイ左端
    //private float distance = 121.0f;    // レイを飛ばす距離
    //private RaycastHit hit;             // レイが何かに当たった時の情報
    //private Vector3 rayPosition;        // レイを発射する位置

    //状態管理用列挙
    enum STATE
    {
        GROUND, //着地
        JUMP,   //ジャンプ
        FALL,   //落下
        DEAD    //死亡
    };

    //スキル選択管理用列挙
    enum SKILL
    {
        WATER,  //水
        ALCOOL, //アルコール
        ACID,   //酸
        FREEZE  //液体窒素(凍らせる液体)
    };

    //アニメーション用状態列挙
    enum ANIM_STATE
    {
        WALK,
        RUN,
        JUMP,
        SHOT,
        STOP,
        WAIT,
        DAMAGE,
        INIT
    }


    //ノックバック用フラグ
    private bool isKnockBack_ = false;

    //自分の体力
    private int hitPoint_ = 200;

    //自分の向き判定
    private int forward_ = 1;

    //ノックバック方向格納用
    private int knockDir_ = 1;

    //地面にいるかどうか判定用フラグ
    private bool isGround_ = true;

    //フレームカウント用
    private int flameCnt_ = 0;

    //ジャンプ用ベクトル変数
    private Vector3 jumpVec_ = new Vector3 (0, 0, 0);

    //状態記憶用変数
    private STATE state_ = STATE.GROUND;

    //スキル選択状態記憶用変数
    private SKILL skillState_;

    //自身のrigitbodyを格納用
    private Rigidbody rigidbody_;

    //ジャンプ力
    public float jump_=600;

    //行動制御用
    private bool operationPossible_ = true;

    //アニメーションステート
    ANIM_STATE animState_ = ANIM_STATE.WAIT;

    //前回選択アニメーションステート
    ANIM_STATE preAnim_ = ANIM_STATE.INIT;

    //アニメーション管理用変数
    PlayerAnim playerAnim_;

    // Start is called before the first frame update
    void Start()
    {
        //アニメーションスクリプトを探す
        playerAnim_ = GetComponent<PlayerAnim>();

        //自分のRigidBody受け取り
        rigidbody_ = this.GetComponent<Rigidbody>();

        jumpVec_.y = JUMP_POWER;
        this.gameObject.transform.Rotate(0, ROTATE_FRONT, 0);

        Vector3 pos = transform.position;

        pos.x = SPAWN_POS;

        //初期位置代入
        transform.position = pos;

        //スキルカーソルオブジェクトを探す
        skillCursor_ = GameObject.Find("SkillCursor");

        //現在選択中のスキル受け取り
        skillState_ = (SKILL)skillCursor_.GetComponent<SkillCursor>().GetSkillCursorStete();
    }

    // Update is called once per frame
    void Update()
    {
        SetAnimation();

        if (operationPossible_ == true)
        {
            PlayerInput();
        }
        else
        {
            animState_ = ANIM_STATE.WAIT;
        }

        //Debug.Log(animState_);
        Debug.Log(isGround_);
    }

    //当たり判定(ぶつかるやつ)
    private void OnCollisionEnter(Collision collision)
    {   
        switch (collision.gameObject.tag)
        {
            //相手がステージだったら
            case "Stage":
                Invoke("SetIsGround", 0.3f);
                break;

        }
    }

    //当たり判定(通り抜けられるやつ)
    private void OnTriggerEnter(Collider collision)
    {
        switch (collision.gameObject.tag)
        {
            //相手が火だったら
            case "Fire":

                //火オブジェクトを探す
                GameObject fire;
                fire = GameObject.Find(collision.gameObject.name);

                //もし火が自分より後ろにいたら
                if(fire.transform.position.x < this.transform.position.x)
                {
                    knockDir_ = 1;
                }
                else
                {
                    knockDir_ = -1;
                }
                isKnockBack_ = true;
                break;
        }
    }   

    //ノックバック関数
    private void KnockBack(int direction)
    {
        //位置格納用
        Vector3 pos;

        //プレイヤーの現在地を格納
        pos = this.transform.position;

        if(isKnockBack_ == true)
        {
            if(flameCnt_ < FLAME_MAX / 2)
            {
                flameCnt_++;

                //Debug.Log("プレイヤーの現在地：" + pos.x);

                //Debug.Log("プレイヤーのノックバック方向：" + 5.0f * direction);

                //引いた分だけノックバック
                pos.x += 8.0f * direction;

                //ノックバック後の位置を代入
                this.transform.position = pos;

                //ダメージモーション
                animState_ = ANIM_STATE.DAMAGE;

                

            }
            else
            {
                isKnockBack_ = false;
                //カウント初期化
                flameCnt_ = 0;
                
            }
        }

    }

    //向きを通知用関数
    public int GetForward()
    {
        return forward_;
    }

    //相手から受けるダメージを通知してもらう関数
    public void ReceiveDamage(int damage)
    {
        hitPoint_ -= damage;
    }

    //他クラスにhitPoint通知用関数
    public int GetHitPoint()
    {
        return hitPoint_;
    }

    //状態通知用
    public int GetState()
    {
        return (int)state_;
    }

    //選択スキル通知用
    public int GetSkillState()
    {
        return (int)skillState_;
    }

    private void PlayerInput()
    {
        //常にフラグをリセット
        //isGround_ = false;

        //選択しているスキルを割り当て
        skillState_ = (SKILL)skillCursor_.GetComponent<SkillCursor>().GetSkillCursorStete();

        /*rayPosition = this.transform.position; // レイを発射する位置の調整
        rayPosition.x += RADIUS-1;
        rayR_ = new Ray(rayPosition, new Vector3(0, -1f, 0)); // レイを下に飛ばす

        if (Physics.Raycast(rayR_, out hit, distance)) // レイが当たった時の処理
        {
            if (hit.collider.tag == "Stage") // レイが地面に触れたら、
            {
                //地面に触れている状態にする
                isGround_ = true;
                state_ = STATE.GROUND;
            }

            else //そうでなければ
            {
                //地面に触れてないことにする
                isGround_ = false;
            }
        }*/

        if(!(animState_==ANIM_STATE.SHOT)&&!(animState_==ANIM_STATE.JUMP)&&!(animState_==ANIM_STATE.DAMAGE))
        {
            animState_ = ANIM_STATE.WAIT;
        }

        //////////////移動処理//////////////

        if (Input.GetKey(KeyCode.D) && isKnockBack_ == false && transform.position.x <= STAGE_R_EDGE)
        {
            //ダッシュボタンが押されていたら
            if (Input.GetKey(KeyCode.LeftShift))
            {
                //右へ移動
                transform.Translate(0, 0, MOVE_SPEED * DASH_POWER);
                if (!(animState_ == ANIM_STATE.JUMP))
                {
                    animState_ = ANIM_STATE.RUN;
                }
            }
            else
            {
                transform.Translate(0, 0, MOVE_SPEED);
                if (!(animState_ == ANIM_STATE.JUMP))
                {
                    animState_ = ANIM_STATE.WALK;
                }
            }

            //後ろを向いていたら
            if (forward_ == -1)
            {
                //向きを前に
                forward_ = 1;
                this.gameObject.transform.Rotate(0, TURN, 0);
            }
        }

        if (Input.GetKey(KeyCode.A) && isKnockBack_ == false && transform.position.x >= STAGE_L_EDGE )
        {
            //ダッシュボタンが押されていたら
            if (Input.GetKey(KeyCode.LeftShift))
            {
                //左へ移動
                transform.Translate(0, 0, MOVE_SPEED * DASH_POWER);
                if (!(animState_ == ANIM_STATE.JUMP))
                {
                    animState_ = ANIM_STATE.RUN;
                }
            }
            else
            {
                //左へ移動
                transform.Translate(0, 0, MOVE_SPEED);
                if (!(animState_ == ANIM_STATE.JUMP))
                {
                    animState_ = ANIM_STATE.WALK;
                }
            }

            //前を向いていたら
            if (forward_ == 1)
            {
                //向きを後ろに
                forward_ = -1;
                this.gameObject.transform.Rotate(0, TURN, 0);
            }
        }

        ////////////ノックバック////////////
        KnockBack(knockDir_);


        //////////////ジャンプ//////////////
        if (Input.GetKeyDown(KeyCode.Space) && isKnockBack_ == false && isGround_ == true)
        {
            animState_ = ANIM_STATE.JUMP;
            rigidbody_.AddForce(Vector3.up * jump_, ForceMode.Impulse);
            isGround_ = false;
        }


        //////////////スキル//////////////
        if (Input.GetKeyDown(KeyCode.K))
        {

            //ジェネレータを探す
            GameObject generator = GameObject.Find("SkillGenerator");

            switch (skillState_)
            {
                //水を選んでいたら
                case SKILL.WATER:

                    Skill();

                    //水オブジェクトを呼ぶ
                    generator.GetComponent<SkillGenerator>().CreateWater();

                    break;

                //アルコールを選んでいたら
                case SKILL.ALCOOL:

                    //アルコールオブジェクトを呼ぶ
                    generator.GetComponent<SkillGenerator>().CreateAlcool();
                    break;

                //酸を選んでいたら
                case SKILL.ACID:

                    Skill();

                    //酸オブジェクトを呼ぶ
                    generator.GetComponent<SkillGenerator>().CreateAcid();
                    break;

                //液体窒素を選んでいたら
                case SKILL.FREEZE:

                    Skill();

                    //液体窒素オブジェクトを呼ぶ
                    generator.GetComponent<SkillGenerator>().CreateFreeze();
                    break;
            }
        }


        //////////////死亡処理//////////////
        if (hitPoint_ <= DEAD_HP)
        {
            //hitPointが0になったら状態を死亡状態にする
            state_ = STATE.DEAD;
        }

        //キャラクター死亡
        if (state_ == STATE.DEAD)
        {
            Destroy(this.gameObject);
        }

        //地面の下に落ちたら死亡
        if (this.transform.position.y < DEAD_LINE)
        {
            Destroy(this.gameObject);
        }
    }

    //操作権の剥奪
    public void Deprivation()
    {
        operationPossible_ = false;
    }

    //操作権の授与
    public void Awarded()
    {
        operationPossible_ = true;
    }

    //ノックバック関数を呼ぶ
    public void CallKnockBack(int direction)
    {
        isKnockBack_ = true;
        knockDir_ = direction;
        KnockBack(direction);
        Debug.Log(direction);
    }

    //アニメーション呼び出し関数
    private void SetAnimation()
    {
        //前回と違うアニメーション呼び出しがあったら
        if(preAnim_ != animState_)
        {
            //選択アニメーションを更新
            preAnim_ = animState_;

            //アニメーションステートを見て
            switch (preAnim_)
            {
                //歩き
                case ANIM_STATE.WALK:
                    playerAnim_.MotionWalk();
                    break;

                //走り
                case ANIM_STATE.RUN:
                    playerAnim_.MotionRun();
                    break;

                //ジャンプ
                case ANIM_STATE.JUMP:
                    playerAnim_.MotionJump();
                    break;

                //スキル
                case ANIM_STATE.SHOT:
                    playerAnim_.MotionShot();
                    break;

                //停止
                case ANIM_STATE.STOP:
                    playerAnim_.MotionStop();
                    break;

                //ダメージ
                case ANIM_STATE.DAMAGE:
                    playerAnim_.MotionDamage();
                    break;

                //何も選択されていない
                case ANIM_STATE.WAIT:
                    playerAnim_.MotionWait();
                    break;
            }
        }

    }

    //モーションをスキルにする
    public void Skill()
    {
        animState_ = ANIM_STATE.SHOT;
    }

    //モーションを待機にする
    public void SetWait()
    {
        animState_ = ANIM_STATE.WAIT;
    }

    //地面との接触判定
    public void SetIsGround()
    {
        isGround_ = true;
    }
}
