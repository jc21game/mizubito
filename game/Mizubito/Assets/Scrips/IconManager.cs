﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;//スクリプトでUI(テキストなど)扱うときはこれ必須！！

public class IconManager : MonoBehaviour
{
    //アイコンをきれいに動かすための移動量
    const float STAGEICON_MOVE = 1900.0f;

    //アイコンをきれいに動かすための移動量
    Vector3 LEFTARROW_POS = new Vector3(50, 540, 0);
    Vector3 RIGHTARROW_POS = new Vector3(1870, 540, 0);
    Vector3 SCREEN_OUT = new Vector3(2000, 2000, 0);

    //各画像を移動させるための変数
    public RectTransform Stage1_1;
    public RectTransform Stage1_2;
    public RectTransform Stage1_3;
    public RectTransform Stage2_1;
    public RectTransform Stage2_2;
    public RectTransform Stage2_3;
    public RectTransform Stage3_1;
    public RectTransform Stage3_2;
    public RectTransform Stage3_3;
    public RectTransform RArrow;
    public RectTransform LArrow;

    //カーソルの情報を取得
    GameObject obj;

    //矢印の位置を取得
    GameObject obj2;

    //スタート関数
    void Start()
    {
        //カーソルをいじれるようにする
        obj = GameObject.Find("Cursor");

        //やじるしをいじれるようにする
        obj2 = GameObject.Find("ArrowIcon");
    }

    //アップデート関数
    void Update()
    {
        AroowIconMovement();
        StageIconMovement();
    }

    //ステージアイコンの移動をまとめた関数
    //引数    :   なし
    //戻り値  :   なし
    private void StageIconMovement()
    {
        //現在のカーソルの位置でアイコンの位置を移動
        switch (obj.GetComponent<Cursor>().GetCursorState())
        {
            //ステージ2-1の状態で→を押したなら下記アイコンを左に移動
            case (int)CURSOR_STATE.STAGE_2_1:

                if (Input.GetKeyUp(KeyCode.RightArrow))
                {
                    IconMovement(-STAGEICON_MOVE);
                }

                break;

            //ステージ3-1の状態で→を押したなら下記アイコンを左に移動
            case (int)CURSOR_STATE.STAGE_3_1:

                if (Input.GetKeyUp(KeyCode.RightArrow))
                {
                    IconMovement(-STAGEICON_MOVE);
                }

                break;

            //ステージ2-3の状態で←を押したなら下記アイコンを右に移動
            case (int)CURSOR_STATE.STAGE_2_3:

                if (Input.GetKeyUp(KeyCode.LeftArrow))
                {
                    IconMovement(STAGEICON_MOVE);
                }

                break;

            //ステージ1-3の状態で←を押したなら下記アイコンを右に移動
            case (int)CURSOR_STATE.STAGE_1_3:

                if (Input.GetKeyUp(KeyCode.LeftArrow))
                {
                    IconMovement(STAGEICON_MOVE);
                }

                break;
        }
    }

    //やじるしをアイコンの移動をまとめた関数
    //引数    :   なし
    //戻り値  :   なし
    private void AroowIconMovement()
    {
        switch (obj2.GetComponent<IconArrow>().GetArrowState())
        {
            case (int)ARROW_STATE.LEFT_ARROW:

                LArrow.position = LEFTARROW_POS;
                RArrow.position = SCREEN_OUT;

                break;
            case (int)ARROW_STATE.BOTH_ARROW:

                LArrow.position = LEFTARROW_POS;
                RArrow.position = RIGHTARROW_POS;

                break;
            case (int)ARROW_STATE.RIGHT_ARROW:

                LArrow.position = SCREEN_OUT;
                RArrow.position = RIGHTARROW_POS;

                break;

        }
    }

    //アイコン移動のためにしていた代入をまとめた関数
    //引数     :   移動量
    //戻り値   :   なし
    private void IconMovement(float move)
    {
        Stage1_1.position += new Vector3(move, 0, 0);
        Stage1_2.position += new Vector3(move, 0, 0);
        Stage1_3.position += new Vector3(move, 0, 0);

        Stage2_1.position += new Vector3(move, 0, 0);
        Stage2_2.position += new Vector3(move, 0, 0);
        Stage2_3.position += new Vector3(move, 0, 0);

        Stage3_1.position += new Vector3(move, 0, 0);
        Stage3_2.position += new Vector3(move, 0, 0);
        Stage3_3.position += new Vector3(move, 0, 0);
    }
}
