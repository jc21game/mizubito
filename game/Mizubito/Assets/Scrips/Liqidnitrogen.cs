﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Liqidnitrogen : MonoBehaviour
{
    //プレイヤー情報
    public GameObject player_;

    //FPS値の最大
    private const int FLAME_MAX = 60;

    //スキル使用時にプレイヤーが受けるダメージ
    private const int DEMERIT_DAMAGE = 5;

    //フレームカウント変数
    private int flameCnt = 0;

    // Start is called before the first frame update
    void Start()
    {
        //プレイヤーオブジェクトを探す
        player_ = GameObject.Find("Player");

        //液体窒素生成時のダメージをプレイヤーに与える
        player_.GetComponent<Player>().ReceiveDamage(DEMERIT_DAMAGE);

    }

    // Update is called once per frame
    void Update()
    {
        //フレームカウント
        flameCnt++;

        //もしフレームカウントが90に達したなら
        if(flameCnt==FLAME_MAX*1.5)
        {
            Destroy(this.gameObject);
        }
    }

    //当たり判定処理
    private void OnTriggerEnter(Collider collider)
    {
        switch (collider.gameObject.tag)
        {
            case "River":

                //川オブジェクトを探す
                GameObject river;
                river = GameObject.Find(collider.gameObject.name);

                //川を凍らせる
                //River.Stop();
                Destroy(this.gameObject);
                break;

            case "Enemy":

                //敵オブジェクトを探す
                GameObject enemy_;
                enemy_ = GameObject.Find(collider.gameObject.name);

                enemy_.GetComponent<Enemy>().SetStop();

                //自分も消す
                Destroy(this.gameObject);
                break;

            case "Fire":

                //炎オブジェクトを消す
                Destroy(collider.gameObject);

                //自分も消える
                Destroy(this.gameObject);
                break;

            //当たり判定無視
            case "Player":
                break;

            //当たり判定無視
            case "Liqidnitrogen":
                break;

            //他のオブジェクトに当たった時
            default:
                Destroy(this.gameObject);
                break;

        }

    }
}
