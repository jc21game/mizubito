﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectStage : MonoBehaviour
{
    //カーソルの情報を取得するためのオブジェクト
    GameObject obj;

    bool changeFlag_ = false;

    // Start is called before the first frame update
    void Start()
    {
        //カーソルを取得
        obj = GameObject.Find("Cursor");
    }

    // Update is called once per frame
    void Update()
    {
        //カーソルの現在のさしている情報を取得
       Cursor c = obj.GetComponent<Cursor>();



        if (Input.GetKeyUp(KeyCode.Space))
        {
            changeFlag_ = true;
        }

        if (changeFlag_)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //カーソルの状態によって移動するステージを決定
                switch (c.GetCursorState())
                {
                    case (int)CURSOR_STATE.STAGE_1_1:
                        SceneManager.LoadScene("1-1");
                        break;
                    case (int)CURSOR_STATE.STAGE_1_2:
                        SceneManager.LoadScene("1-1");
                        break;
                    case (int)CURSOR_STATE.STAGE_1_3:
                        SceneManager.LoadScene("1-1");
                        break;
                    case (int)CURSOR_STATE.STAGE_2_1:
                        SceneManager.LoadScene("Stage_1-1");
                        break;
                }
            }
        }

    }
}
