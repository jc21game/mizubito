﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverEffect : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle;
    // 1. 再生
    private void Play()
    {
        particle.Play();
    }

    // 2. 一時停止
    private void Pause()
    {
        particle.Pause();
    }

    // 3. 停止
    private void Stop()
    {
        particle.Stop();
    }

    //川
    //private GameObject river_=null;

    //凍っているか
    private bool FREEZE;

    public void Freeze()
    {
        Stop();
    }

}
