﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    //プレイヤー情報
    public GameObject player_;

    //FPS値の最大
    private const int FLAME_MAX = 60;

    //スキル使用時にプレイヤーが受けるダメージ
    private const int DEMERIT_DAMAGE = 5;

    //フレームカウント用変数
    private int flameCnt = 0;

    // Start is called before the first frame update
    void Start()
    {
        //プレイヤーオブジェクトを探す
        player_ = GameObject.Find("Player");

        //仮で水オブジェクトの色を青色に設定
        //this.GetComponent<Renderer>().material.color = Color.blue;
        //マテリアルを適用しました

        //水生成時のダメージをプレイヤーに与える
        player_.GetComponent<Player>().ReceiveDamage(DEMERIT_DAMAGE);
    }

    // Update is called once per frame
    void Update()
    {
        //フレームカウント
        flameCnt++;

        //もしフレームカウントが90に達したなら
        if(flameCnt == FLAME_MAX * 1.5)
        {
            //自分を消す
            Destroy(this.gameObject);
        }
    }

    //当たり判定処理
    private void OnTriggerEnter(Collider collider)
    {

        //当たった相手によて処理を分岐
        switch (collider.gameObject.tag)
        {
            case "Fire":

                //炎オブジェクトを消す
                Destroy(collider.gameObject);

                //自分も消える
                Destroy(this.gameObject);
                break;

            //当たり判定無視
            case "Player":
                break;

            //当たり判定無視
            case "Water":
                break;

            //他オブジェクトに当たった時
            default:

                //自分を消す
                Destroy(this.gameObject);
                break;
        }
        
    }
}
