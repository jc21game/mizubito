﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverDrive : MonoBehaviour
{
    //流れる川の長さ
    private const float riverSpeed=0.8f;

    GameObject player_;

    // Start is called before the first frame update
    void Start()
    {
        //プレイヤーオブジェクトを探す
        player_ = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, riverSpeed));
    }

    //当たり判定処理
    private void OnTriggerEnter(Collider coll)
    {

        switch (coll.gameObject.tag)
        {
        
            case "Player":

                player_.GetComponent<Player>().CallKnockBack(-1);

                break;

            //他のオブジェクトに当たった時
            default:
                break;
        }

    }
}
