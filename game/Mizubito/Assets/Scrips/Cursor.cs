﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum CURSOR_STATE : int
{
    STAGE_1_1,
    STAGE_1_2,
    STAGE_1_3,
    STAGE_2_1,
    STAGE_2_2,
    STAGE_2_3,
    STAGE_3_1,
    STAGE_3_2,
    STAGE_3_3,
    STAGE_MAX
};

public class Cursor : MonoBehaviour
{
    //X軸のCursor移動量
    private const float CURSOR_MOVE_X  = 600.0f;

    //600pxごとに画像配置しているため画面端に〇-1の画像が少し移ってしまう
    //その問題を修正するための変数
    private const float CURSOR_MOVE_FIX_X = 128.0f;

    //現在のcursorがさしているもの
    private int cursorState_ = (int)CURSOR_STATE.STAGE_1_1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //「→」を押したなら
        if (Input.GetKeyUp(KeyCode.RightArrow) && cursorState_ != (int)CURSOR_STATE.STAGE_3_3)
        {
            // transformを取得
            Transform myTransform = this.transform;

            // 座標を取得
            Vector3 pos = myTransform.position;

            //カーソルが端まできたらもとに戻す
            switch (cursorState_)
            {
                case (int)CURSOR_STATE.STAGE_1_3:

                    // 右方向にカーソルを移動
                    pos.x += CURSOR_MOVE_X * -2;

                    break;

                case (int)CURSOR_STATE.STAGE_2_3:

                    // 右方向にカーソルを移動
                    pos.x += CURSOR_MOVE_X * -2;

                    break;

                default:

                    // 右方向にカーソルを移動
                    pos.x += CURSOR_MOVE_X;

                    break;
            }

            //値を代入
            myTransform.position = pos;

            //Cursorの状態を変更
            cursorState_++; 
        }
        
        //「←」を押したなら
        if (Input.GetKeyUp(KeyCode.LeftArrow) && cursorState_ != (int)CURSOR_STATE.STAGE_1_1)
        {
            // transformを取得
            Transform myTransform = this.transform;

            // 座標を取得
            Vector3 pos = myTransform.position;

            //カーソルの状態が2-1と3-1ならカメラが移動するため移動量を増やす
            switch (cursorState_)
            {
                case (int)CURSOR_STATE.STAGE_2_1:

                    // 左方向にカーソルを移動
                    pos.x += CURSOR_MOVE_X * 2;

                    break;

                case (int)CURSOR_STATE.STAGE_3_1:

                    // 左方向にカーソルを移動
                    pos.x += CURSOR_MOVE_X * 2;

                    break;

                default:

                    // 左方向にカーソルを移動
                    pos.x -= CURSOR_MOVE_X;

                    break;
            }

            //値を代入
            myTransform.position = pos;

            //Cursorの状態を変更
            cursorState_--;
        }
    }

    ///カーソルの状態を取得
    ///引数   : なし
    ///戻り値 : カーソルの状態
    public int GetCursorState()
    {
        return cursorState_;
    }
}
