﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SkillIconManager : MonoBehaviour
{
    GameObject waterImage_;         //水アイコンのゲームオブジェクト
    GameObject alcoholImage_;       //アルコールアイコンのゲームオブジェクト
    GameObject acidImage_;          //酸アイコンのゲームオブジェクト
    GameObject freezeImage_;        //氷アイコンのゲームオブジェクト

    //スキルの数のよりスキルの表示位置が変わるのでその位置の定数を定義
    const float SKILL_ICON_HEIGHT = -250.0f;                        //スキルアイコンの下からの高さ
    
    Vector3[] SKILL_ICON_COUNT_4 = { new Vector3(-180, SKILL_ICON_HEIGHT, 0), new Vector3(-60, SKILL_ICON_HEIGHT, 0),    new Vector3(60, SKILL_ICON_HEIGHT, 0),      new Vector3(180, SKILL_ICON_HEIGHT, 0) };
    Vector3[] SKILL_ICON_COUNT_3 = { new Vector3(-200, SKILL_ICON_HEIGHT, 0), new Vector3(0, SKILL_ICON_HEIGHT, 0),      new Vector3(200, SKILL_ICON_HEIGHT, 0) };
    Vector3[] SKILL_ICON_COUNT_2 = { new Vector3(-100, SKILL_ICON_HEIGHT, 0), new Vector3(100, SKILL_ICON_HEIGHT, 0) };
    Vector3 SKILL_ICON_COUNT_1 = new Vector3(0, SKILL_ICON_HEIGHT, 0);

    string[] fileName_1 = { "Temp_Skill_Water", "Temp_Skill_ICON", "Temp_Skill_ICON", "Temp_Skill_ICON" };           //水、アルコール、酸、氷
    string[] fileName_2 = { "Temp_Skill_Water", "Temp_Skill_Alcohol", "Temp_Skill_Acid", "Temp_Skill_Freeze" };       //水、アルコール、酸、氷

    // Start is called before the first frame update
    void Start()
    {
        //器のゲームオブジェクトを作成
        waterImage_     = new GameObject("WorkImage");
        alcoholImage_   = new GameObject("WorkImage2");
        acidImage_      = new GameObject("WorkImage3");
        freezeImage_    = new GameObject("WorkImage4");

        //現在のシーンで読み込む画像を決定！
        switch (SceneManager.GetActiveScene().name)
        {
            case "1-1":

                //アイコンを生成
                SkillIconLoad((int)SKILL_COUNT.SKILL_COUNT_2);

                break;

            case "1-2":

                //アイコンを生成
                SkillIconLoad((int)SKILL_COUNT.SKILL_COUNT_2);

                break;

            case "1-3":

                //アイコンを生成
                SkillIconLoad((int)SKILL_COUNT.SKILL_COUNT_2);

                break;
            case "TEST_PlayScene":

                //アイコンを生成
                SkillIconLoad((int)SKILL_COUNT.SKILL_COUNT_2);

                break;

            case "Stage_1-1":

                //アイコンを生成
                SkillIconLoad((int)SKILL_COUNT.SKILL_COUNT_2);

                break;


        }

    }

    //アイコンをまとめて生成します
    //引数    : 使用するスキルの数
    //戻り値  :  なし
    public void SkillIconLoad(int StageType)
    {
        switch (StageType)
        {
            case 3:

                IconLoad(waterImage_, fileName_2[0], SKILL_ICON_COUNT_4[0]);
                IconLoad(alcoholImage_, fileName_2[1], SKILL_ICON_COUNT_4[1]);
                IconLoad(acidImage_, fileName_2[2], SKILL_ICON_COUNT_4[2]);
                IconLoad(freezeImage_, fileName_2[3], SKILL_ICON_COUNT_4[3]);

                break;

            case 2:

                IconLoad(waterImage_, fileName_2[0], SKILL_ICON_COUNT_4[0]);
                IconLoad(alcoholImage_, fileName_2[1], SKILL_ICON_COUNT_4[1]);
                IconLoad(acidImage_, fileName_2[2], SKILL_ICON_COUNT_4[2]);

                break;

            case 1:

                IconLoad(waterImage_, fileName_2[0], SKILL_ICON_COUNT_4[0]);
                IconLoad(alcoholImage_, fileName_2[1], SKILL_ICON_COUNT_4[1]);
                IconLoad(acidImage_, fileName_2[2], SKILL_ICON_COUNT_4[2]);
                IconLoad(freezeImage_, fileName_2[3], SKILL_ICON_COUNT_4[3]);

                break;
            case 0:

                IconLoad(waterImage_, fileName_2[0], SKILL_ICON_COUNT_4[0]);
                IconLoad(alcoholImage_, fileName_2[1], SKILL_ICON_COUNT_4[1]);
                IconLoad(acidImage_, fileName_2[2], SKILL_ICON_COUNT_4[2]);
                IconLoad(freezeImage_, fileName_2[3], SKILL_ICON_COUNT_4[3]);

                break;
        }

        
    }


    // Update is called once per frame
    void Update()
    {

   
    }

    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void IconLoad(GameObject workImage,string fileName,Vector3 pos)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find("Canvas").transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

    //表示されているスキルアイコンを削除します
    //引数    : 　なし
    //戻り値  :   なし  
    public void SkillIconDelete()
    {
        Destroy(waterImage_);
        Destroy(alcoholImage_);
        Destroy(acidImage_);
        Destroy(freezeImage_);
    }
}
