﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum MOTION_PLAYER
{
    MOTION_NONE = 0,
    MOTION_WALK,    
    MOTION_RUN,
    MOTION_JUMP,
    MOTION_SHOT,
    MOTION_WAIT,
    MOTION_DAMAGE
}

public class PlayerAnim : MonoBehaviour
{
    //int trans;

    Animator animator;

    // アニメーションの開始時間
    float animStartTime_ = 0;

    // Start is called before the first frame update
    void Start()
    {
        //GetComponentを用いてAnimatorコンポーネントを取り出す.
        animator = GetComponent<Animator>();
        //animator.enabled = trans;
    }

    // Update is called once per frame
    void Update()
    {   
        //Qを押したときにアニメーションを"歩く"にする
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log(1);
            MotionWalk();
        }

        //Wを押したときにアニメーションを"走る"にする
        if (Input.GetKeyDown(KeyCode.W))
        {
            Debug.Log(2);
            MotionRun();

        }

        //Eを押したときにアニメーションを"ジャンプ"にする
        if (Input.GetKeyDown(KeyCode.E))
        { 
            Debug.Log(3);
            MotionJump();
        }

        //Rを押したときにアニメーションを"スキル"にする
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log(4);
            MotionShot();
        }

        //Tを押したときにアニメーションを"待機"にする
        if (Input.GetKeyDown(KeyCode.T))
        {
            MotionWait();
        }

        //Yを押したときにアニメーションを"ダメージ"にする
        if(Input.GetKeyDown(KeyCode.Y))
        {
            MotionDamage();
        }
        
    }

    public void MotionWalk()
    {
        animator.Play("Walk", 0, 0);
    }

    public void MotionRun()
    {
        animator.Play("run", 0, 0);
    }

    public void MotionJump()
    {
        animator.Play("jump", 0, 0);
    }

    public void MotionShot()
    {
        animator.Play("shot", 0, 0);
    }

    public void MotionWait()
    {
        animator.Play("Wait", 0, 0);
    }

    public void MotionDamage()
    {
        animator.Play("Damage", 0, 0);
    }

    public void SetMotion(int a)
    {
        int A = a;
    }

    public void MotionStop()
    {
        animator.enabled = false;
        animStartTime_ = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

}
