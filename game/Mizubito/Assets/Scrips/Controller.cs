﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    //操作する対象のオブジェクトたち
    enum OPERATION_TARGET : int
    {
        PLAYER,     //プレイヤー
        UI,         //結果のUI
        TARGET_MAX,
    }

    //操作対象
    private OPERATION_TARGET operationTarget_;

    //操作対象のオブジェクト
    private GameObject[] targetObject_ = new GameObject[(int)OPERATION_TARGET.TARGET_MAX];

    //ゴールを格納
    private GameObject goal_ = null;

    // Start is called before the first frame update
    void Start()
    {
        //最初に操作する対象はUI
        operationTarget_ = OPERATION_TARGET.PLAYER;

        //プレイヤーを探して格納する
        targetObject_[(int)OPERATION_TARGET.PLAYER] = GameObject.Find("Player");

        //リザルトUIマネージャーを取得
        targetObject_[(int)OPERATION_TARGET.UI] = GameObject.Find("ResultUIManager");

        //ゴールを取得
        goal_ = GameObject.Find("Goal");
    }

    // Update is called once per frame
    void Update()
    {
        //プレイヤーがゴールと接触していて且つ、操作対象がプレイヤーの時
        if (goal_.GetComponent<Goal>().GetHitFlag() && operationTarget_ == OPERATION_TARGET.PLAYER)
        {
            //プレイヤーの操作権を剥奪する
            targetObject_[(int)OPERATION_TARGET.PLAYER].GetComponent<Player>().Deprivation();

            //操作対象の変更（結果：クリア）
            ChangeOperationTarget("Clear");
        }
        //プレイヤーが死んだ時且つ、操作対象がプレイヤーの時
        else if(targetObject_[(int)OPERATION_TARGET.PLAYER] == null && operationTarget_ == OPERATION_TARGET.PLAYER)
        {
            Debug.Log("ゲームオ－バー");

            //操作対象の変更（結果：ゲームオーバー）
            ChangeOperationTarget("Gameover");
        }
    }

    //操作対象を変更する（結果を引数でもらう）
    public void ChangeOperationTarget(string result)
    {
        //操作対象をUIにする
        operationTarget_ = OPERATION_TARGET.UI;

        //リザルトUI管理クラスの関数を呼び出す（引数に結果が入ってくるので、その画像を表示）
        targetObject_[(int)OPERATION_TARGET.UI].GetComponent<ResultUIManager>().CreateImage(result);

        //UIマネージャーに操作権を与える
        targetObject_[(int)OPERATION_TARGET.UI].GetComponent<ResultUIManager>().GetPermission();
    }


}
