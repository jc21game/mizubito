﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FloorText : MonoBehaviour
{
    //テキストオブジェクト
    public GameObject floorTextObject_;

    //テキストオブジェクトを入れる変数
    private Text floorText_;

    // Start is called before the first frame update
    void Start()
    {
        floorText_ = floorTextObject_.GetComponent<Text>();
        ChangeText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ChangeText()
    {
        //現在のシーンの名前を参照
        switch(SceneManager.GetActiveScene().name)
        {
            case "1-1":
                floorText_.text = "研究所 地下";
                break;

            case "1-2":
                floorText_.text = "研究所 地下";
                break;

            case "1-3":
                floorText_.text = "研究所 地下";
                break;

            default:
                break;

        }
    }

    private void DeleteText()
    {
        floorText_.text = "";
    }

    public void SetDeleteText()
    {
        DeleteText();
    }
}
