﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleSceneChange : MonoBehaviour
{
    //string nextScene_ = "SelectStageScene";

    string nextScene_ = "1-1";

    bool changeFlag_ = false;

    GameObject titleLogo_;

    // Start is called before the first frame update
    void Start()
    {
        titleLogo_ = GameObject.Find("TitleLogo");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (changeFlag_)
            {
                SceneManager.LoadScene(nextScene_);
            }

            changeFlag_ = true;
            titleLogo_.GetComponent<TitleIndicate>().SetIndicate();
        }   

        if(Input.GetKeyDown(KeyCode.F1))
        {
            SceneManager.LoadScene("StaffCredit");
        }
    }
}
