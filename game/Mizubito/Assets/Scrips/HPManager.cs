﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPManager : MonoBehaviour
{
    //スライダーの情報を取得
    public Slider slider_;

    //プレイヤーの情報を習得
    GameObject player_;

    //HPの最大値
    int MAX_HP = 200;

    //プレイヤーが死ぬ高さ
    private const float DEAD_LINE = -5;

    //現在のHP
    int currentHP_ = 0;

    // Start is called before the first frame update
    void Start()
    {
        player_ = GameObject.Find("Player");

        //Sliderを満タンにする。
        slider_.value = 1;

        //現在のHPを最大HPと同じに。
        currentHP_ = MAX_HP;
    }

    // Update is called once per frame
    void Update()
    {
        //ダメージを決定
        int damage = Random.Range(1,5);

        if(player_ != null)
        {
            //ダメージを受ける
            currentHP_ = player_.GetComponent<Player>().GetHitPoint();

            //現在のHPをHPバーに繁栄
            slider_.value = (float)currentHP_ / (float)MAX_HP;

            //プレイヤーが落下死したとき
            if (player_.GetComponent<Player>().transform.position.y < DEAD_LINE)
            {
                //自分も消える
                Destroy(this.gameObject);
            }
        }
    }
}
