﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alcool : MonoBehaviour
{
    //スピード
    private float MOVE_SPEED;

    //ダメージ
    private int DAMAGE = 5;

    //フレーム
    private int FLAME = 60;

    //アルコール棒の状態管理用列挙
    enum STATE
    {
        STANDERD,
        IGNITION
    }

    //スキル選択管理用列挙
    enum SKILL
    {
        WATER,
        ALCOOL,
        ACID,
        MAX
    };

    //半径
    private const float RADIUS = 60.0f;

    //プレイヤー情報
    public GameObject player_;

    //状態情報
    private STATE state_ = STATE.STANDERD;

    //投げられているかどうか判定用
    private bool isThrow_ = false;

    //自分が投げられた位置記憶用
    private Vector3 throwPos_;

    //フレームカウント用
    private int flameCnt_ = 0;

    // Start is called before the first frame update
    void Start()
    {
        //仮で水オブジェクトの色を黄色に設定
        //this.GetComponent<Renderer>().material.color = Color.yellow;
    }

    // Update is called once per frame
    void Update()
    {
        //持っている間の処理
        if (isThrow_ == false)
        {
            //プレイヤーオブジェクトを探す
            player_ = GameObject.Find("Player");

            //プレイヤーが死んだら自分も死ぬ
            if (player_ == null)
            {
                Destroy(this.gameObject);
            }

            //プレイヤーが別スキルを選んだらアルコールを消す
            if (player_.GetComponent<Player>().GetSkillState() == (int)SKILL.WATER)
            {
                Destroy(this.gameObject);
            }

            //プレイヤーposition受け取り
            Vector3 pos = player_.transform.position;

            //プレイヤーpositionに向きと半径を掛けたものを足す
            pos.x += (RADIUS * 2) * player_.GetComponent<Player>().GetForward();
            pos.y += RADIUS+60;

            //位置
            this.transform.position = pos;
        }

        //持っていて、スキルボタンが押された時の処理
        if(Input.GetKeyDown(KeyCode.K) && isThrow_ == false)
        {
            player_.GetComponent<Player>().Skill();

            player_.GetComponent<Player>().ReceiveDamage(DAMAGE);

            //アルコールの進行方向
            Vector3 force;

            float forward;

            //向きにスピードを掛ける
            forward = player_.GetComponent<Player>().GetForward();

            force = new Vector3(forward, 0, 0);

            //スピードを設定
            this.GetComponent<Rigidbody>().AddForce(force * MOVE_SPEED);

            //フラグをオンにする
            isThrow_ = true;

            //投げられた位置を記憶
            throwPos_ = transform.position;
        }

        //投げられてる時の処理
        if(isThrow_ == true)
        {
            //一定距離飛んだら
            if (Vector3.Distance(throwPos_, transform.position) > 600.0f)
            {
                //自身を削除
                Destroy(this.gameObject);
            }
        }

        //燃えていて手に持っているときの処理
        if(state_ == STATE.IGNITION && isThrow_ == false)
        {
            flameCnt_++;
            if (flameCnt_ == FLAME)
            {
                flameCnt_ = 0;
                //player_.GetComponent<Player>().ReceiveDamage(DAMAGE);
            }
        }
    }

    //当たり判定処理
    private void OnTriggerEnter(Collider coll)
    {

        //当たった相手によて処理を分岐
        switch (coll.gameObject.tag)
        {
            //相手が火だったら
            case "Fire":

                //自分の状態を発火状態にする
                state_ = STATE.IGNITION;

                this.GetComponent<Renderer>().material.color = Color.red;

                break;
            
            //相手が爆弾だったら
            case "Bomb":

                //爆弾オブジェクトを探す
                GameObject bomb;
                bomb = GameObject.Find(coll.gameObject.name);

                //自分が発火していたら
                if (state_ == STATE.IGNITION)
                {
                    //爆弾に着火する
                    bomb.GetComponent<Bomb>().Ignite();
                }
                break;

            //当たり判定無視
            case "Player":
                break;

            //当たり判定無視
            case "Alcohol":
                break;

            //他のオブジェクトに当たった時
            default:
                //投げられてたら
                if (isThrow_ == true)
                {
                    //自分を消す
                    Destroy(this.gameObject);
                }
                break;
        }

    }

    public void SetSpeed(float speed)
    {
        MOVE_SPEED = speed;
    }

    //状態を渡す（アルコールのエフェクトから呼ばれる）
    public int GetState()
    {
        return (int)state_;
    }
}
