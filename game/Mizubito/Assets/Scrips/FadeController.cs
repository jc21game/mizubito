﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeController : MonoBehaviour
{
    //透明度の変わるスピード
    float fadeSpeed = 0.03f;

    //パネルの色、不透明度を管理
    float red, green, blue, alfa;

    //フェードアウト
    public bool isFadeIn = false;

    //透明度を変更するパネル
    Image fadeImage_;

    //フレーム数を計測
    private float flameCount;
    
    //
    private GameObject floorTextManager_;

    private GameObject player_;

    // Start is called before the first frame update
    void Start()
    {
        //フェード用の色相設定
        fadeImage_ = GetComponent<Image>();
        red = fadeImage_.color.r;
        green = fadeImage_.color.g;
        blue = fadeImage_.color.b;
        alfa = fadeImage_.color.a;

        //シーンの名前を変更するManagerを探す
        floorTextManager_ = GameObject.Find("FloorTextManager");

        //プライヤーを探す
        player_ = GameObject.Find("Player");

        //プレイヤーの操作権を剥奪
        player_.GetComponent<Player>().Deprivation();
    }

    // Update is called once per frame
    void Update()
    {
        flameCount++;

        if(flameCount>60)
        {
            StartFadeIn();
        }
    }

    void StartFadeIn()
    {

        //透明度を徐々に上げる
        alfa -= fadeSpeed;

        //透明度をパネルに代入
        SetAlpha();

        //完全に透明になったら処理を止めてプレイヤーに操作権を与える
        if(alfa<=0)
        {
            isFadeIn = false;

            //パネルの表示をオフにする
            fadeImage_.enabled = false;

            //階層テキストを消す
            floorTextManager_.GetComponent<FloorText>().SetDeleteText();

            player_.GetComponent<Player>().Awarded();

            Destroy(this.gameObject);

        }

    }

    void SetAlpha()
    {
        fadeImage_.color = new Color(red, green, blue, alfa);
    }
}
