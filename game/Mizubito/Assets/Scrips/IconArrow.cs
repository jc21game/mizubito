﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum ARROW_STATE : int
{
    RIGHT_ARROW,
    BOTH_ARROW,
    LEFT_ARROW
};

public class IconArrow : MonoBehaviour
{
    //カーソルの情報を取得
    GameObject obj;

    //矢印のアイコンの状態を格納
    private int arrowState_ = (int)ARROW_STATE.RIGHT_ARROW;

    // Start is called before the first frame update
    void Start()
    {
        //カーソルをいじれるようにする
        obj = GameObject.Find("Cursor");
    }

    // Update is called once per frame
    void Update()
    {
        //現在のカーソルの位置で矢印の表示状態を変更
        switch (obj.GetComponent<Cursor>().GetCursorState())
        {
            case (int)CURSOR_STATE.STAGE_2_1:

                arrowState_ = (int)ARROW_STATE.BOTH_ARROW;

                break;

            case (int)CURSOR_STATE.STAGE_2_3:

                arrowState_ = (int)ARROW_STATE.BOTH_ARROW;

                break;
            
            case (int)CURSOR_STATE.STAGE_1_3:

                arrowState_ = (int)ARROW_STATE.RIGHT_ARROW;

                break;
            case (int)CURSOR_STATE.STAGE_3_1:

                arrowState_ = (int)ARROW_STATE.LEFT_ARROW;

                break;

        }
    }


    ///矢印の状態を取得
    ///引数   : なし
    ///戻り値 : 矢印の状態
    public int GetArrowState()
    {
        return arrowState_;
    }
}

