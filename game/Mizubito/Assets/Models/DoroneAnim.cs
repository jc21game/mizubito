﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoroneAnim : MonoBehaviour
{
    Animator anim;

    // アニメーションの開始時間
    //float animStartTime_ = 0;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.Play("Take 001", 0, 0);
        Debug.Log("稼働中");
    }

}
