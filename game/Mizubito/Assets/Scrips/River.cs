﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class River : MonoBehaviour
{
    //フレームカウント用変数
    //private int flameCnt = 0;

    //川のエフェクト
    private GameObject riverEffect_ = null;

    //オブジェクトのコライダーを複数取得
    private BoxCollider[] collider_;

    //氷のPrefab
    public GameObject icePrefab_;

   

    // Start is called before the first frame update
    void Start()
    {
        //川のエフェクトを取得
        riverEffect_ = GameObject.Find("RiverEffect");

        //川のコライダーを取得
        collider_ = GetComponents<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //当たり判定
    private void OnTriggerEnter(Collider collision)
    {
        //対象のオブジェクトの名前を取得
        switch (collision.gameObject.tag)
        {
            
            //液体窒素に当たったなら
            case "Liqidnitrogen":

                //水のパーティクルを止める
                //riverEffect_.GetComponent<RiverEffect>().Freeze();
                Destroy(this.gameObject);

                //タグをステージに変更
                this.gameObject.tag = "Stage";

                CreateIce();

                for (int i =0;i<collider_.Length;i++)
                {
                    collider_[i].isTrigger = false;
                }
                break;
        }
    }

    private void CreateIce()
    {
        //川の座標を取得
        Transform myTransform = this.transform;

        Vector3 pos = myTransform.position;

        Instantiate(icePrefab_, new Vector3(pos.x-341,pos.y-119,0), Quaternion.AngleAxis(18,Vector3.forward));
    }
    
}
