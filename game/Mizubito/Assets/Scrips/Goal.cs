﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : MonoBehaviour
{
    //プレイヤーと衝突したかどうか
    private bool isPlayerHit_ = false;

    GameObject player_;

    // Start is called before the first frame update
    void Start()
    {
        //わかりやすく色を変更
        //this.GetComponent<Renderer>().material.color = Color.black;

        //プレイヤーオブジェクトを探す
        player_ = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //当たり判定
    private void OnTriggerEnter(Collider collision)
    {
        //対象がプレイヤーなら
        if (collision.gameObject.name == "Player")
        {
            //わかりやすく色を変更
            //this.GetComponent<Renderer>().material.color = Color.green;

            player_.GetComponent<Player>().SetWait();

            //ゴールに触れたかの確認用
            Debug.Log("ゴール!");

            //プレイヤーと衝突した
            isPlayerHit_ = true;

        }
    }

    //プレイヤーと衝突したかどうかを渡す
    public bool GetHitFlag()
    {
        return isPlayerHit_;
    }
}
