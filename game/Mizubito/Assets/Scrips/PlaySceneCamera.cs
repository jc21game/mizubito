﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySceneCamera : MonoBehaviour
{
    //カメラのZ軸
    private const float CAM_Z_POS = -100.0f;

    //カメラのY軸
    private const float CAM_Y_POS = 340.0f;

    //画面の半分
    private const float SCREEN_HALF = 960.0f;

    //画面の端
    private const float SCREEN_EDGE = 14000.0f;

    //プレイヤーとの距離
    private const float PLAYER_DIST = SCREEN_HALF - 420;

    //プレイヤー位置保存用
    Vector3 pos_ = new Vector3(0, 0, 0);

    //プレイヤー情報受け取り用
    private GameObject player_;

    enum STATE
    {
        L_EDGE,
        R_EDGE,
        NONE
    }

    private STATE state_ = STATE.NONE;

    // Start is called before the first frame update
    void Start()
    {
        player_ = GameObject.Find("Player");

        //位置格納用
        Vector3 pos_;

        //プレイヤーのポジションを受け取り
        pos_ = player_.transform.position;

        //Z軸修正
        pos_.z = CAM_Z_POS;

        //X軸修正
        pos_.x += PLAYER_DIST;

        //Y軸修正
        pos_.y = CAM_Y_POS;

        //カメラの位置に代入
        this.transform.position = pos_;
    }

    // Update is called once per frame
    void Update()
    {
        //自分の位置受け取り
        Vector3 myPos = this.transform.position;

        

        //プレイヤーがいる間
        if (player_ != null)
        {
            //プレイヤー位置受け取り
            pos_ = player_.transform.position;
            //Y軸修正
            pos_.y = CAM_Y_POS;
            myPos.y = pos_.y;
        }


        this.transform.position = myPos;

        //もしプレイヤーがいて、画面が両端まで移動してないとき
        if (player_ != null && myPos.x <= SCREEN_HALF && state_ == STATE.NONE)
        {
            //左端フラグを立てる
            state_ = STATE.L_EDGE;
        }
        else if(myPos.x >= SCREEN_EDGE - SCREEN_HALF && state_ == STATE.NONE)
        {
            //右端フラグを立てる
            state_ = STATE.R_EDGE;
        }

        //画面が左端でプレイヤーが右側に移動したとき
        if (player_ != null && pos_.x + PLAYER_DIST >= SCREEN_HALF  && state_ == STATE.L_EDGE)
        {
            //Z軸修正
            pos_.z = CAM_Z_POS;

            //x軸修正
            pos_.x += PLAYER_DIST;

            //カメラの位置に代入
            this.transform.position = pos_;

            state_ = STATE.NONE;
        }
        //画面が右端でプレイヤーが左側に移動したとき
        else if (player_ != null && pos_.x <= SCREEN_EDGE - PLAYER_DIST - SCREEN_HALF && state_ == STATE.R_EDGE)
        {
            //Z軸修正
            pos_.z = CAM_Z_POS;

            //x軸修正
            pos_.x += PLAYER_DIST;

            //カメラの位置に代入
            this.transform.position = pos_;

            state_ = STATE.NONE;
        }
        else if(player_ != null && state_ == STATE.NONE)
        {
            //Z軸修正
            pos_.z = CAM_Z_POS;

            //x軸修正
            pos_.x += PLAYER_DIST;

            //カメラの位置に代入
            this.transform.position = pos_;
        }

    }
}
