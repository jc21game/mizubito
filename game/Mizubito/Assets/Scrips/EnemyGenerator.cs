﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyGenerator : MonoBehaviour
{
    //経過時間
    //private float searchTime = 0;

    //ドローンのPrefab
    public GameObject enemyPrefab_;

    //生成できる状態か
    private bool spawn=true;

    GameObject[] tagObjects;

    //フレームカウント用
    private int flameCnt_ = 0;

    GameObject player_;

    Vector3 playerPosition_;

    // Start is called before the first frame update
    void Start()
    {
        CreateDorone();
    }

    // Update is called once per frame
    void Update()
    {
        EnemyCheck("Enemy");

        //Transform Target;
 
        if(spawn)
        {
            flameCnt_++;
            if(flameCnt_>120)
            {
                CreateDorone();

                flameCnt_ = 0;
            }
            
        }
    }

    private void EnemyCheck(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);
        //Debug.Log(tagObjects.Length); 

    }
    public void CreateDorone()
    {
        try
        {
            player_ = GameObject.FindWithTag("Player");
            playerPosition_= player_.transform.position;
        }
        catch(NullReferenceException ex)
        {

        }

        if (playerPosition_.x>7000)
        {
            Instantiate(enemyPrefab_, transform.position, Quaternion.identity);
            spawn = false;
        }
    }

    public void ChangeSpawn()
    {
        spawn = true;
    }
}