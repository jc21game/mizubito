﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverGenerator : MonoBehaviour
{
    //水のPrefab
    public GameObject water;

    //氷
    public GameObject ice;

    //FPS値の最大
    private const int FLAME_MAX = 60;

    //フレームカウント用変数
    private int flameCnt = 0;

    //川が流れているか
    public bool flow = true;

    //川のエフェクト
    private GameObject riverEffect_=null;

    //水のprefabを作成
    void CreateRiver()
    {
        Instantiate(water, new Vector3(7580.0f, 360.0f, 0.0f), Quaternion.identity);
    }

    // Start is called before the first frame update
    void Start()
    {
        riverEffect_ = GameObject.Find("RiverEffect");
    }

    // Update is called once per frame
    void Update()
    { 
        //川が流れているか
        if (flow)
        {
            flameCnt++;
            if (flameCnt == 20)
            {
                CreateRiver();
                flameCnt = 0;
            }
        }

    }

    public void Freeze()
    {
        //川の水を止める
        flow = false;

    }

}
