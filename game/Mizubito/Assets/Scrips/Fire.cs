﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    //火の状態の定数
    enum STATE
    {
        STATE_BURN, //燃える
        STATE_MORE_BURN,//勢いが強まる
        STATE_WEAK, //勢いが弱まる
    }

    //プレイヤーに与えるダメージ
    private const int DAMAGE = 20;

    /*    これから使うかも
    //「燃える」モーションの始め
    private const int BURN_MOTION_START = 0;

    //「燃える」モーションの終わり
    private const int BURN_MOTION_END = 0;

    //「消える」モーションの始め
    private const int WEAK_MOTION_START = 0;

    //「消える」モーションの終わり
    private const int WEAK_MOTION_END = 0;
    */

    //火の状態
    private STATE state_;

    //プレイヤー
    private GameObject player_ = null;

    // Start is called before the first frame update
    void Start()
    {
        //プレイヤーを探して格納する
        player_ = GameObject.Find("Player");
        
        //最初から燃える
        state_ = STATE.STATE_BURN;

        //わかりやすく色を変更
        //this.GetComponent<Renderer>().material.color = Color.red;

    }

    // Update is called once per frame
    void Update()
    {
        //本来は動かないが試作のため移動
        //transform.Translate(0.01f, 0.0f, 0.0f);


        //「火の状態」に対応するモーションを再生


        if(Input.GetKey(KeyCode.U))
        {
            Vector3 collider = this.GetComponent<BoxCollider>().size;

            collider *= 2;

            this.GetComponent<BoxCollider>().size = collider;
        }

    }

    //当たり判定
    private void OnTriggerEnter(Collider collision)
    {
        //対象のオブジェクトの名前を取得
        switch (collision.gameObject.name)
        {
            //対象がプレイヤーの時は
            case "Player":
                //ダメージを与える
                Debug.Log(collision.gameObject.name + "に" + DAMAGE + "ダメージ");

                if (player_ != null)
                {
                    //プレイヤーのHPを減らす（おそらくSet関数を呼ぶ）
                    player_.GetComponent<Player>().ReceiveDamage(DAMAGE);
                }
                else
                {
                    Debug.Log("プレイヤーは居ません。");
                }
                
                break;
        }
    }

    //火の状態を変更する（衝突した相手から呼ばれる）
    public void ChangeState(string targetName)
    {
        //衝突した相手
        switch (targetName)
        {
            //水なら
            case "Water":
                //勢いが弱まり、やがて消える
                state_ = STATE.STATE_WEAK;
                break;
            
            
            //アルコールなら
            case "Alcool":
                //勢いがさらに強まる
                state_ = STATE.STATE_MORE_BURN;
                break;

            //酸なら
            case "Acid":

                state_ = STATE.STATE_WEAK;
                break;

            //液体窒素なら
            case "Liqidnitrogen":

                state_ = STATE.STATE_WEAK;
                break;
            
        }
    }
}
