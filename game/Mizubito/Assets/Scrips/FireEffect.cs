﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEffect : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle;
    // 1. 再生
    private void Play()
    {
        particle.Play();
    }

    // 2. 一時停止
    private void Pause()
    {
        particle.Pause();
    }

    // 3. 停止
    private void Stop()
    {
        particle.Stop();
    }
    public GameObject alcoolprefab_;
    private int BURN;

    // Start is called before the first frame update
    void Start()
    {
        Stop();
        BURN = alcoolprefab_.GetComponent<Alcool>().GetState();
    }

    // Update is called once per frame
    void Update()
    {
        BURN=alcoolprefab_.GetComponent<Alcool>().GetState();
        if (BURN==0 )
        {
            Stop();
        }

        if (BURN == 1)
        {
            Play();
        }
    }
}
