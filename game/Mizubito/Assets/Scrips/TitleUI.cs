﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleUI : MonoBehaviour
{
    //透明度が変わるスピード
    float fadeSpeed = 0.016f;

    //パネルの色と透明度を管理
    float red, green, blue, alfa;

    //フェードイン
    public bool fadeIn_=false;

    //透明度を変更するパネル
    Image titleImage_;

    private GameObject title_;

    // Start is called before the first frame update
    void Start()
    {
        //フェード用の色相設定
        titleImage_ = GetComponent<Image>();
        red = titleImage_.color.r;
        green = titleImage_.color.g;
        blue = titleImage_.color.b;
        alfa = titleImage_.color.a;

        //タイトルを探して格納する
        title_ = GameObject.Find("TitleLogo");

    }

    // Update is called once per frame
    void Update()
    {

        if (title_.GetComponent<TitleIndicate>().fadeIn_==false)
        {
            if(fadeIn_)
            {
                StartFadeIn();
            }
            else
            {
                StartFadeOut();
            }
        }
    }

    private void StartFadeIn()
    {
        //透明度を徐々に下げる
        alfa += fadeSpeed;

        //透明度をパネルに代入
        SetAlpha();

        //完全に表示されたら処理を止める
        if (alfa >=1)
        {
            fadeIn_ = false;
        }

    }

    private void StartFadeOut()
    {

        //透明度を徐々に上げる
        alfa -= fadeSpeed;

        //透明度をパネルに代入
        SetAlpha();

        //完全に透明になったら処理を止める
        if (alfa <= 0)
        {
            fadeIn_ = true;
        }

    }

    private void SetAlpha()
    {
        titleImage_.color = new Color(red, green, blue, alfa);
    }


}
