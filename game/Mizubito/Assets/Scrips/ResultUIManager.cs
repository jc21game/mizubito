﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class ResultUIManager : MonoBehaviour
{
    //状態
    enum CURSOR_POSITION
    {
        RIGHT,  //右
        LEFT    //左
    }

    //リザルトの画像
    private GameObject resultImage_ = null;

    //カーソルの画像
    private GameObject cursorImage_ = null;

    //リザルト画像に足し合わせる数値（y値）
    private const int RESULT_IMAGE_ADD_Y = 50;

    //リザルト画像のz値
    private const int RESULT_IMAGE_Z = -62;

    //カーソル画像に足し合わせる数値（x値）
    private const int CURSOR_IMAGE_ADD_X = 600;

    //カーソル画像に足し合わせる数値（y値）
    private const int CURSOR_IMAGE_ADD_Y = 360;

    //カーソル画像のz値
    private const int CURSOR_IMAGE_Z = -61;

    //ステージの名前
    //private List<string> stageName_ = new List<string>();
    private string[] stageName_ = new string[16];

    //操作できるかどうか
    private bool operationPossible_ = false;

    // Start is called before the first frame update
    void Start()
    {
        //DirectoryInfo dir = new DirectoryInfo(Application.dataPath + "/Scenes");
        //FileInfo[] info = dir.GetFiles("Stage_*.unity");
        //Debug.Log("ResultUIManager.Start()");

        //string filePath = Path.GetFileNameWithoutExtension(Application.dataPath + "/Scenes");

        //foreach (FileInfo file in info)
        //{

        //}

        int arrayCount = 0;

        for (int i = 1; i < 5; i++)
        {
            for(int j = 1; j < 5; j++)
            {
                stageName_[arrayCount] =i + "-" + j;

                arrayCount++;
            }
        }

        //stageName_[0] = "TEST_PlayScene";
        stageName_[0] = "1-1";
    }

    // Update is called once per frame
    void Update()
    {
        //操作する許可が出ているなら
        if(operationPossible_)
        {
            //入力に対する反応をする
            UIInput();
        }
    }

    //入力関数
    private void UIInput()
    {
        //今のステージをもらう
        //結果がどうだったか（ゲームオーバーorクリア）
        //カーソルの位置が左右どちらか

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject cursorObject = GameObject.Find("Temp_ResultCursorUI(Clone)");

            int cursorPos = cursorObject.GetComponent<ResultCursor>().GetPosition();

            //結果を見て
            switch (resultImage_.name)
            {
                //クリアなら
                case "Temp_ClearUI":

                    for (int i = 0; i < 9; i++)
                    {
                        //今のシーンと同じシーンを見つけたら
                        if (stageName_[i] == SceneManager.GetActiveScene().name)
                        {
                            //現在のシーンが1-3なら
                            if (SceneManager.GetActiveScene().name=="1-3")
                            {
                                SceneChange(cursorPos, "TitleScene");
                                break;
                            }

                            //i++;
                            //次のシーン（ステージ）名を渡す
                            SceneChange(cursorPos, stageName_[++i]);

                            break;
                        }
                    }

                    break;

                //ゲームオーバーなら
                case "Temp_GameoverUI":
                    //今のシーン名を渡す
                    SceneChange(cursorPos, SceneManager.GetActiveScene().name);
                    break;

                default:
                    Debug.Log("該当なし");
                    break;
            }
        }
    }

    //画像を生成する
    public void CreateImage(string imageName)
    {
        //引数で渡された画像の名前を見る
        switch (imageName)
        {
            case "Clear":
                // ResourcesフォルダにあるクリアUIのプレハブをGameObject型で取得
                resultImage_ = (GameObject)Resources.Load("Temp_ClearUI");
                break;

            case "Gameover":
                // ResourcesフォルダにあるゲームオーバーUIのプレハブをGameObject型で取得
                resultImage_ = (GameObject)Resources.Load("Temp_GameoverUI");
                break;
        }

        //カメラの現在位置を取得
        GameObject camera = GameObject.Find("Main Camera");
        Vector3 resultImagePos = camera.transform.position;

        //位置の調整
        resultImagePos.y -= RESULT_IMAGE_ADD_Y; 
        resultImagePos.z = RESULT_IMAGE_Z;

        // 上記で作成したプレハブを元に、インスタンスを生成
        Instantiate(resultImage_, resultImagePos, Quaternion.identity);

        // ResourcesフォルダにあるカーソルプレハブをGameObject型で取得
        cursorImage_ = (GameObject)Resources.Load("Temp_ResultCursorUI");

        Vector3 resultCursorPos = resultImagePos;

        //位置の調整
        resultCursorPos.x -= CURSOR_IMAGE_ADD_X;
        resultCursorPos.y -= CURSOR_IMAGE_ADD_Y;
        resultCursorPos.z = CURSOR_IMAGE_Z;

        // カーソルプレハブを元に、インスタンスを生成
        Instantiate(cursorImage_, resultCursorPos, Quaternion.identity);

        GameObject skillIconManager = GameObject.Find("SkillIconManager");

        //スキルアイコンを消去
        skillIconManager.GetComponent<SkillIconManager>().SkillIconDelete();

        GameObject skillCursor = GameObject.Find("SkillCursor");
        skillCursor.GetComponent<SkillCursor>().CursorDelete(); //よんでおいたよbyしょうた
    }

    //シーンを切り替える
    public void SceneChange(int cursorPos, string sceneName)
    {

        //カーソルの位置を見て
        switch (cursorPos)
        {
            //右にいるなら
            case (int)CURSOR_POSITION.RIGHT:
                //引数で渡されたシーンに遷移
                SceneManager.LoadScene(sceneName);
                break;

            //左にいるなら
            case (int)CURSOR_POSITION.LEFT:
                //タイトルシーンに遷移
                SceneManager.LoadScene("TitleScene");
                break;
        }
    }

    //許可をもらう
    public void GetPermission()
    {
        operationPossible_ = true;
    }
}