﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

enum SKILL_STATE
{
    SKILL_WATER,
    SKILL_ALCOHOL,
    SKILL_ACID,
    SKILL_FREEZE
}

enum SKILL_COUNT
{
    SKILL_COUNT_1,
    SKILL_COUNT_2,
    SKILL_COUNT_3,
    SKILL_COUNT_4
}


public class SkillCursor : MonoBehaviour
{
    //SKILL_COUNT test = SKILL_COUNT.SKILL_COUNT_1;

    Vector3 SKILL_POS_1 = new Vector3(-180, -250, -1);   //スキル数が1個の場合のカーソル初期位置
    Vector3 SKILL_POS_2 = new Vector3(-180, -250, -1);   //スキル数が2個の場合のカーソル初期位置
    Vector3 SKILL_POS_3 = new Vector3(-180, -250, -1);   //スキル数が3個の場合のカーソル初期位置
    Vector3 SKILL_POS_4 = new Vector3(-180, -250, -1);   //スキル数が4個の場合のカーソル初期位置

    const float MOVE = 214.0f;                          //カーソルの移動量

    GameObject cursorImage_;                            //カーソルのゲームオブジェクトを入れるやつ

    int stageSkillCount = 0;                            //現在のスキル数

    int skillState_ = (int)SKILL_STATE.SKILL_WATER;     //カーソルが選択しているスキル

    Dictionary<string, SKILL_COUNT> skillDictionary = new Dictionary<string, SKILL_COUNT>()
     {
        {"TEST3_PlayScene",SKILL_COUNT.SKILL_COUNT_4},
        {"TEST2_PlayScene",SKILL_COUNT.SKILL_COUNT_3},
        {"TEST_PlayScene",SKILL_COUNT.SKILL_COUNT_2},
        {"Stage_1-1",SKILL_COUNT.SKILL_COUNT_2},
        {"1-1",SKILL_COUNT.SKILL_COUNT_4},
        {"1-2",SKILL_COUNT.SKILL_COUNT_4},
        {"1-3",SKILL_COUNT.SKILL_COUNT_4},
     };


    void Start()
    {
        //カーソルのゲームオブジェクトを生成
        cursorImage_ = new GameObject("WorkImage5");

        SetCursorPoS((int)skillDictionary[SceneManager.GetActiveScene().name]);

    }

    // Update is called once per frame
    void Update()
    {
        ////////////////////////////////////////////////                        ///////////////////////////////////////////////
        ///////////////////////////////////////////////     カーソルの移動処理   ///////////////////////////////////////////////
        ///////////////////////////////////////////////                        ///////////////////////////////////////////////
        //;スキルの数でカーソルの移動を制限
        switch (stageSkillCount)
        {
            ///あとからCursorよみこめば


            //スキルの数が４個なら
            case (int)SKILL_COUNT.SKILL_COUNT_4:

                //Lをおして右端のスキルを選択していなかったら
                if (Input.GetKeyUp(KeyCode.L) && skillState_ != (int)SKILL_STATE.SKILL_FREEZE)
                {
                    //カーソルの移動先を変更
                    cursorImage_.transform.Translate(MOVE, 0, 0);
                    skillState_++;

                }

                //Jをおして左端のスキルを選択していなかったら
                if (Input.GetKeyUp(KeyCode.J) && skillState_ != (int)SKILL_STATE.SKILL_WATER)
                {
                    //Cursorの移動先
                    cursorImage_.transform.Translate(-MOVE, 0, 0);
                    skillState_--;
                }

                break;

            //スキルの数が3個なら
            case (int)SKILL_COUNT.SKILL_COUNT_3:

                //Lをおして右端のスキルを選択していなかったら
                if (Input.GetKeyUp(KeyCode.L) && skillState_ != (int)SKILL_STATE.SKILL_ACID)
                {
                    //カーソルの移動先を変更
                    cursorImage_.transform.Translate(MOVE, 0, 0);
                    skillState_++;

                }

                //Jをおして左端のスキルを選択していなかったら
                if (Input.GetKeyUp(KeyCode.J) && skillState_ != (int)SKILL_STATE.SKILL_WATER)
                {
                    //Cursorの移動先
                    cursorImage_.transform.Translate(-MOVE, 0, 0);
                    skillState_--;
                }

                break;

            //スキルの数が２こなら
            case (int)SKILL_COUNT.SKILL_COUNT_2:

                //Lをおして右端のスキルを選択していなかったら
                if (Input.GetKeyUp(KeyCode.L) && skillState_ != (int)SKILL_STATE.SKILL_ALCOHOL)
                {
                    //カーソルの移動先を変更
                    cursorImage_.transform.Translate(MOVE, 0, 0);
                    skillState_++;

                }

                //Jをおして左端のスキルを選択していなかったら
                if (Input.GetKeyUp(KeyCode.J) && skillState_ != (int)SKILL_STATE.SKILL_WATER)
                {
                    //Cursorの移動先
                    cursorImage_.transform.Translate(-MOVE, 0, 0);
                    skillState_--;
                }

                break;
        }




    }
    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void IconLoad(GameObject workImage, string fileName, Vector3 pos)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find("Canvas").transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

    //カーソルの状態を取得
    //引数    :   なし
    //戻り値  :   カーソルのさしているもの
    public int GetSkillCursorStete()
    {
        return skillState_;
    }

    //カーソルを削除
    //引数    :   なし
    //戻り値  :   なし
    public void CursorDelete()
    {
        Destroy(cursorImage_);
    }

    //引数によってCursorの位置を変更
    //引数    :  ステージのスキル数
    //戻り値  :  なし
    public void SetCursorPoS(int sKillCount)
    {
        switch (sKillCount)
        {
            case 3:

                //カーソルのさしているものを決定
                skillState_ = (int)SKILL_STATE.SKILL_WATER;

                //表示するスキルの数を決定
                IconLoad(cursorImage_, "Temp_Skill_Cursor", SKILL_POS_4);

                //スキルの数を決定
                stageSkillCount = (int)SKILL_COUNT.SKILL_COUNT_4;

                break;
            case 2:

                //カーソルを生成
                skillState_ = (int)SKILL_STATE.SKILL_WATER;

                //表示するスキルの数を決定
                IconLoad(cursorImage_, "Temp_Skill_Cursor", SKILL_POS_3);

                //スキルの数を決定
                stageSkillCount = (int)SKILL_COUNT.SKILL_COUNT_3;

                break;
            case 1:

                //カーソルのさしているものを決定
                skillState_ = (int)SKILL_STATE.SKILL_WATER;

                //表示するスキルの数を決定
                IconLoad(cursorImage_, "Temp_Skill_Cursor", SKILL_POS_2);

                //スキルの数を決定
                stageSkillCount = (int)SKILL_COUNT.SKILL_COUNT_2;

                break;
            case 0:

                //カーソルのさしているものを決定
                skillState_ = (int)SKILL_STATE.SKILL_WATER;

                //表示するスキルの数を決定
                IconLoad(cursorImage_, "Temp_Skill_Cursor", SKILL_POS_1);

                //スキルの数を決定
                stageSkillCount = (int)SKILL_COUNT.SKILL_COUNT_1;

                break;
        }
    }

    public void SetStageSkillCount(int count)
    {
        stageSkillCount = count;
    }
}