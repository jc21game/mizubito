﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    //通常の状態がある
    //火が付くと「点火」に
    //水と接触すると「通常」に
    //プレイヤーとはぶつからない
    //爆破範囲は半径120pxの円系

    //爆弾の状態を指す
    enum STATE
    {
        NORMAL,
        IGNITION,
        EXPLOSION
    }

    //爆発するまでの時間
    private const int UNTIL_THE_EXPLOSION = 180;

    //消える（死ぬ）までの時間
    private const float UNTIL_DEAD = 0.5f;

    //プレイヤーに与えるダメージ
    private const int DAMAGE = 20;

    //爆弾の状態
    private STATE state_ = STATE.NORMAL;

    //爆発までのフレームをカウント
    private int untilExplosionCount_ = 0;

    //爆発した時のコライダーのサイズ
    private float explosionColliderSize_ = 2.0f;

    //衝突したタグを格納
    private ArrayList hitTag_ = new ArrayList();

    //最も近いオブジェクト
    private GameObject nearObject;

    //爆発エフェクト
    public GameObject explostion_;

    //火が付くエフェクト
    public GameObject bombFire_;

    //時間経過
    //private float searchTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        //わかりやすく色を変更
        this.GetComponent<Renderer>().material.color = Color.black;

        hitTag_.Clear();

        //とりあえず適当な名前を入れておく
        hitTag_.Add("InitObj");

        //Debug.Log("爆弾カウント：" + hitTag_.Count);

        //for (int i = 0; i < hitTag_.Count; i++)
        //{
        //    Debug.Log("爆弾初期：" + hitTag_[i]);
        //}

        Init();

    }

    // Update is called once per frame
    void Update()
    {
        //点火状態なら
        if (state_ == STATE.IGNITION)
        {
            //エフェクトを火が付いた状態にする
            bombFire_.GetComponent<BombFire>().Ignition();

            //爆発するまでの時間が経過していないなら
            if (untilExplosionCount_ < UNTIL_THE_EXPLOSION)
            {
                untilExplosionCount_++;
            }
            //経過したなら
            else
            {
                state_ = STATE.EXPLOSION;
                BoxCollider boxCol = GetComponent<BoxCollider>();
                Destroy(boxCol);

            }
        }
        else if (state_ == STATE.EXPLOSION)
        {

            //Debug.Log("爆発します");

            SphereCollider sphereCol = GetComponent<SphereCollider>();

            //Debug.Log("爆発前：" + boxCol.size);

            //Vector3 colSize = new Vector3(5.0f, 5.0f, 5.0f);
            //float newRadius = 2.0f;

            //boxCol.size = colSize;
            sphereCol.radius = explosionColliderSize_;

            //Debug.Log("爆発後：" + boxCol.size);

            //BombEffect.csを取得してエフェクトを一回だけ実行
            explostion_.GetComponent<BombEffect>().Exploued();
            
            //時間をかけて死ぬ
            Destroy(this.gameObject, UNTIL_DEAD);
        }
    }

    //当たり判定
    public void OnTriggerEnter(Collider other)
    {
        //衝突相手が水なら
        if (other.name == "WaterPrefab(Clone)")
        {
            //初期状態に戻す
            Init();

            //わかりやすく色を変更
            this.GetComponent<Renderer>().material.color = Color.blue;

            Debug.Log("爆弾：水と接触");

            bombFire_.GetComponent<BombFire>().Extinguishing();

            //当たったことを記憶
            hitTag_.Add(other.name);

        }
        else if (state_ == STATE.EXPLOSION && other.name == "Player")
        {
            //過去衝突したオブジェクトの名前と、今当たったオブジェクトの名前が違うなら
            if (!(IsExitSameName(other.name)))
            {
                HitPlayer();

                //当たったことを記憶
                hitTag_.Add(other.name);

                Debug.Log("爆弾OnTriggerEnter：プレイヤーと接触");
            }

            //Debug.Log("爆弾[" + i + "]：" + (string)hitTag_[i]);
            //Debug.Log("爆弾：" + other.name);

        }
    }

    //当たり判定
    public void OnTriggerStay(Collider other)
    {
        //爆発している状態且つ、衝突相手がプレイヤーなら
        if (state_ == STATE.EXPLOSION && other.name == "Player")
        {
            //過去衝突したオブジェクトの名前と、今当たったオブジェクトの名前が違うなら
            if (!(IsExitSameName(other.name)))
            {
                HitPlayer();

                //プレイヤーと当たったことを記憶
                hitTag_.Add(other.name);

                Debug.Log("爆弾OnTriggerStay：プレイヤーと接触");
            }
        }

        //Debug.Log("爆弾：" + (string)hitTag_[i]);
        //Debug.Log("爆弾：" + other.name);
    }

    //初期の状態にする（戻す時も呼ぶ）
    private void Init()
    {
        untilExplosionCount_ = 0;
        state_ = STATE.NORMAL;
    }

    //点火する（火のついたアルコールから呼んでもらう）
    public void Ignite()
    {
        //点火状態に移行
        state_ = STATE.IGNITION;

        //わかりやすく色を変更
        //this.GetComponent<Renderer>().material.color = Color.red;
    }

    //名前一覧の中に引数で渡された名前と同じ名前があるかどうかを判断する
    private bool IsExitSameName(string hitObjName)
    {
        for (int i = 0; i < hitTag_.Count; i++)
        {
            //Debug.Log("爆弾[" + i + "]：" + (string)hitTag_[i]);

            //過去衝突したオブジェクトの名前と、今当たったオブジェクトの名前が同じなら
            if ((string)hitTag_[i] == hitObjName)
            {
                //同じ名前があった
                return true;
            }
        }

        //同じ名前は無かった
        return false;
    }

    //プレイヤーと衝突した時
    private void HitPlayer()
    {
        GameObject player = GameObject.Find("Player");

        //プレイヤーにダメージを与える
        player.GetComponent<Player>().ReceiveDamage(DAMAGE);

        //ノックバックさせる方向
        int knockDir = 0;

        //プレイヤーが自分より左に居たら
        if(player.transform.position.x < this.transform.position.x)
        {
            knockDir = -1;
        }
        else
        {
            knockDir = 1;
        }

        Debug.Log("爆弾からプレイヤーへ：" + knockDir);

        //方向を指定してノックバックさせる
        player.GetComponent<Player>().CallKnockBack(knockDir);
    }
    
}