﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillGenerator : MonoBehaviour
{
    //スピード
    public float MOVE_SPEED = 1000000.0f;

    //半径
    private const float RADIUS = 120.0f;

    //水のprefab
    public GameObject waterPrefab_;

    //アルコールのprefab
    public GameObject alcoolPrefab_;

    //酸のprefab
    public GameObject acidPrefab_;

    //液体窒素のprefab
    public GameObject freezePrefab_;

    //プレイヤー情報
    public GameObject player_;

    //アルコールが生成されているか？
    private bool alcoolFlag_ = false;

    //進行方向
    Vector3 force;

    float forward;

    // Start is called before the first frame update
    void Start()
    {
        player_ = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //アルコールオブジェクトを探す
        GameObject alcool = GameObject.Find("AlcoolPrefab(Clone)");

        //もしアルコールがなかったら
        if(alcool == null)
        {
            //アルコールフラグをリセット
            alcoolFlag_ = false;
        }
    }

    //水生成用関数
    public void CreateWater()
    {
        //水の生成
        GameObject water = Instantiate(waterPrefab_) as GameObject;

        //向きにスピードを掛ける
        forward = player_.GetComponent<Player>().GetForward();

        force = new Vector3(forward, 0, 0);

        //スピードを設定
        water.GetComponent<Rigidbody>().AddForce(force * MOVE_SPEED);

        //プレイヤーposition受け取り
        Vector3 pos = player_.transform.position;

        //プレイヤーpositionに向きと半径を掛けたものを足す
        pos.x += RADIUS * player_.GetComponent<Player>().GetForward();
        pos.y += RADIUS;

        //位置
        water.transform.position = pos;
    }


    //アルコール生成用関数
    public void CreateAlcool()
    {
        if (alcoolFlag_ == false)
        {
            //アルコールの生成
            GameObject alcool = Instantiate(alcoolPrefab_) as GameObject;

            //アルコールフラグをオンにする
            alcoolFlag_ = true;

            //プレイヤーposition受け取り
            Vector3 pos = player_.transform.position;

            //プレイヤーpositionに向きと半径を掛けたものを足す
            pos.x += RADIUS * player_.GetComponent<Player>().GetForward();
            pos.y += RADIUS;

            //位置
            alcool.transform.position = pos;

            //スピード設定    
            alcool.GetComponent<Alcool>().SetSpeed(MOVE_SPEED);

            Debug.Log(alcoolFlag_);
        }
    }

    //酸生成用関数
    public void CreateAcid()
    {
        //酸を生成
        GameObject acid = Instantiate(acidPrefab_) as GameObject;

        //向きにスピードを掛ける
        forward = player_.GetComponent<Player>().GetForward();

        force = new Vector3(forward, 0, 0);

        //プレイヤーposition受け取り
        Vector3 pos = player_.transform.position;

        //プレイヤーpositionに向きと半径をかけたものを足す
        pos.x += RADIUS * player_.GetComponent<Player>().GetForward();
        pos.y += RADIUS;

        //位置
        acid.transform.position = pos;

        //スピード設定
        acid.GetComponent<Rigidbody>().AddForce(force * MOVE_SPEED);
    }

    //液体窒素生成用関数
    public void CreateFreeze()
    {
        //液体窒素を生成
        GameObject freeze = Instantiate(freezePrefab_) as GameObject;

        //向きにスピードを掛ける
        forward = player_.GetComponent<Player>().GetForward();

        force = new Vector3(forward, 0, 0);

        //プレイヤーposition受け取り
        Vector3 pos = player_.transform.position;

        //プレイヤーpositionに向きと半径をかけたものを足す
        pos.x += RADIUS * player_.GetComponent<Player>().GetForward();
        pos.y += RADIUS;

        //位置
        freeze.transform.position = pos;

        //スピード設定
        freeze.GetComponent<Rigidbody>().AddForce(force * MOVE_SPEED);
    }
}
