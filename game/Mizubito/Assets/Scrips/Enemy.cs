﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //オブジェクトの座標
    private Vector3 targetpos;

    //オブジェクトの直前の座標
    private Vector3 lastestpos;

    //氷のPrefab
    public GameObject icePrefab_;

    //消える（死ぬ）までの時間
    private const float UNTIL_DEAD = 0.01f;

    //敵の状態
    enum STATE
    {
        PATROL,
        FREEZE
    }

    //敵の状態を収容する関数
    private STATE state_;

    Transform Target;
    GameObject Player;
    GameObject enemyGenerator_;

    //敵とぶつかったときにプレイヤーが受けるダメージ
    private const int DAMAGE = 20;

    //アニメーター
    private Animator doroneAnim_;

    // Start is called before the first frame update
    void Start()
    {
        //オブジェクトの座標を取得
        targetpos = transform.position;

        //プレイヤータグの取得
        Player = GameObject.FindWithTag("Player");
        Target = Player.transform;

        //状態を巡回にする
        state_ = STATE.PATROL;

        //アニメーターを取得
        doroneAnim_ = GetComponent<Animator>();

        //ジェネレーターを取得
        enemyGenerator_ = GameObject.Find("EnemyGenerator");

    }

    // Update is called once per frame
    void Update()
    {
        //敵の状態が巡回なら
        if (state_==STATE.PATROL)
        {
            //Sin関数のグラフに合わせて座標を変更
            transform.position = new Vector3(Mathf.Sin(Time.time) * 500.0f + targetpos.x, targetpos.y, targetpos.z);

            //前回からどこまで進んだか;
            Vector3 diff = transform.position - lastestpos;
            lastestpos = transform.position;

            //ベクトルの大きさが-の時に向きを変える
            if (diff.magnitude > -0.01f)
            {
                transform.rotation = Quaternion.LookRotation(diff);
            }

        }
    }

    //当たり判定処理
    private void OnCollisionEnter(Collision collision)
    {
        //当たったオブジェクトのタグを表示
        //Debug.Log(collision.gameObject.tag);

        //当たった相手によて処理を分岐
        switch (collision.gameObject.tag)
        {
            case "Player":

               //敵の状態が巡回なら
               if(state_==STATE.PATROL)
               {
                    //プレイヤーオブジェクトを探す
                    GameObject player_;
                    player_ = GameObject.Find("Player");

                    //プレイヤーにダメージ
                    player_.GetComponent<Player>().ReceiveDamage(DAMAGE);

                    //ノックバックさせる方向
                    int knockDir = 0;

                    //プレイヤーが自分より左に居たら
                    if (player_.transform.position.x < this.transform.position.x)
                    {
                        knockDir = -1;
                    }
                    else
                    {
                        knockDir = 1;
                    }

                    //プレイヤーをノックバックさせる
                    player_.GetComponent<Player>().CallKnockBack(knockDir);
               }
                break;

            //液体窒素に当たったら凍る
            case "Liqidnitrogen":

                Stop();
                break;

            //凍っている敵に当たったら消える
            case "Enemy":
                Debug.Log("凍っている敵");
                break;

            //他オブジェクトに当たった時
            default:

                //何も起こらない
                break;
        }

    }

    private void OnDisable()
    {
        try
        {
            Debug.Log("溶ける");

            enemyGenerator_.GetComponent<EnemyGenerator>().ChangeSpawn();
        }
        catch(MissingReferenceException ex)
        {
        }
        
    }
    //凍った時の処理
    private void Stop()
    {
        //tagをステージに変更
        this.gameObject.tag = "Stage";

        //敵を凍っている状態にする
        state_ = STATE.FREEZE;

        //アニメーションを止める
        doroneAnim_.speed = 0;

        //オブジェクトの位置に氷を生成
        CreateIce();
    }

    //呼び出し用
    public void SetStop()
    {
        Stop();
    }

    //氷を生成
    private void CreateIce()
    {
        Instantiate(icePrefab_, new Vector3(transform.position.x,transform.position.y+20,transform.position.z), Quaternion.identity);
    }
}