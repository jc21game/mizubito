﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombFire : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle;
    // 1. 再生
    private void Play()
    {
        particle.Play();
    }

    // 2. 一時停止
    private void Pause()
    {
        particle.Pause();
    }

    // 3. 停止
    private void Stop()
    {
        particle.Stop();
    }
    // Start is called before the first frame update
    void Start()
    {
        Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Ignition()
    {
        Play();
    }

    public void Extinguishing()
    {
        Stop();
    }
}
