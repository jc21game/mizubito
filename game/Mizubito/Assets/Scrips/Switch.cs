﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{

    //経過時間
    private int count_ = 0;

    //火のprefab
    public GameObject firePrefab_;

    //スイッチのONOffを判別する
    enum STATE
    {
        ON,
        OFF
    }

    //スイッチの状態
    private STATE state_;

    GameObject[] tagObjects;

    void Start()
    {
        //色を変更
        this.GetComponent<Renderer>().material.color = Color.red;

        //スイッチをOFFにしておく
        state_ = STATE.OFF;
    }

    void Update()
    {
        //スイッチがONなら
        if (state_ == STATE.ON)
        {
            //ONの状態の経過時間をカウントする
            count_++;

            //カウントが120以上なら
            if(count_>120)
            {
                //transformを取得
                Transform myTransform = this.transform;

                //スイッチの座標をy軸方向に移動
                Vector3 pos = myTransform.position;
                pos.y += 30.0f;

                //座標を再設定
                myTransform.position = pos;

                //カウントを0にする
                count_ = 0;

                //スイッチをOFFにする
                state_ = STATE.OFF;
            }
        }
    }

    //火を生成する
    void CreateFire()
    {
        Instantiate(firePrefab_, new Vector3(1920.0f, 120.0f, 0.0f), Quaternion.identity);
        Instantiate(firePrefab_, new Vector3(5640.0f, 120.0f, 0.0f), Quaternion.identity);
    }

    //シーン上のFireオブジェクトの個数を調べる
    private void FireCheck(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);
    }

    //オブジェクトと衝突した時の処理
    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.name)
        {
            //相手がプレイヤーだったら
            case "Player":

                //transformを取得
                Transform myTransform = this.transform;

                //スイッチがOFFなら
                if (state_ == STATE.OFF)
                {
                    //スイッチの座標を-y軸方向に移動
                    Vector3 pos = myTransform.position;
                    pos.y += -30.0f;

                    //座標を再設定
                    myTransform.position = pos;

                    //火を生成する
                    CreateFire();

                    //スイッチをONにする
                    state_ = STATE.ON;
                }
                break;

        }
    }
    
} 